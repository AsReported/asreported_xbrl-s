Public Function ProcessInstance(oDocument As CDocument, grStatus As Control, oJob As CJob, oToolbox As CToolbox, oCompany As CCompany, oUSGaap As CUsgaap)
'------------------------------------------------
' Author      : R.Santoski
' Date         : 6/23/2015
' Purpose    :
'------------------------------------------------
'DECLARATIONS
'------------------------------------------------
   'Excel Objects..........................
      Dim frm As UserForm
      Dim domDocument As New MSXML2.DOMDocument30
      Dim grReadout As Control
      Dim dctConcepts As Dictionary
   'Custom Objects.......................
      Dim oInterface As CInterface
      Dim oPLinkbase As CPresentationLinkbase
      Dim oCLinkbase As CCalculationLinkbase
      Dim oLLinkbase As CLabelLinkbase
      Dim oElementList As CxElementList
      Dim oPLink As CPresentationLink
      Dim oPArc As CPresentationArc
      Dim oCLink As CCalculationLink
      Dim oCArc As CCalculationArc
      Dim oConcept As CConcept
      Dim oLinkToConcept As CConcept
      Dim oLinkFromConcept As CConcept
      Dim oCalcParentConcept As CConcept
      Dim oLocator As CLocator
      Dim oLabelGroup As CLabelGroup
      Dim oLabelLink As CLabelLink
      Dim oFact As CFact
      Dim oFact1 As CFact
      Dim oContext As CContext
      Dim oSegment As CSegment
      Dim oLinkToLocator As CLocator
      Dim oLinkFromLocator As CLocator
      Dim oCalculationLocator As CLocator
      Dim oLabel As CLabel
      Dim oDataTable As CDataTable
      Dim oLineItem As CLineItem
      Dim oPeriod As CPeriod
   'Database.................................
      Dim rs As ADODB.Recordset
      Dim cmd As ADODB.Command
   'Long.......................................
      Dim iTableCount As Long
      Dim iLinkToConceptID As Long
      Dim iLinkFromConceptID As Long
      Dim iRowCount As Long
      Dim iFactCount As Long
      Dim iArcsTotal As Long
      Dim iDimensionsTotal As Long
      Dim iFactsTotal As Long
      Dim iSegCount As Long
      Dim iCalculationParentID As Long
      Dim iExtCount As Long
      Dim iTotalExtCount As Long
      Dim iFactID As Long
   'Double....................................
   'Single.....................................
   'Boolean...................................
      Dim bComplete As Boolean
      Dim bLabelSaved As Boolean
      Dim bReturn As Boolean
      Dim bLinkFromConceptExists As Boolean
      Dim bISAssigned As Boolean
      Dim bSkipFact As Boolean
      Dim bDefLinkbaseExists As Boolean
      Dim bCalcLinkbaseExists As Boolean
   'String......................................
      Dim sSource As String
      Dim sDestination As String
      Dim sAccession As String
      Dim sPrmQExclusion(1 To 2) As String
      Dim sLabelType As String
      Dim sLabelCode As String
   'Date.......................................
   'Variant....................................
      Dim vPresLink As Variant
      Dim vPresArc As Variant
      Dim vKey As Variant
      Dim vLocator As Variant
'------------------------------------------------
'SET CONDITIONS
'------------------------------------------------
   On Error GoTo ProcessInstance_Error
'------------------------------------------------
'CONSTRUCTORS
'------------------------------------------------
   Set oPLinkbase = New CPresentationLinkbase
   Set oCLinkbase = New CCalculationLinkbase
   Set oLLinkbase = New CLabelLinkbase
   Set oElementList = New CxElementList
   Set oLabel = New CLabel
   Set rs = New ADODB.Recordset
   Set cmd = New ADODB.Command
   Set oCompany.extensions = New Dictionary
   Set oCompany.segments = New Dictionary
   Set oCompany.dimensionArcs = New Dictionary
   Set oCompany.segmentsByID = New Dictionary
   Set oInterface = New CInterface
   Set oConcept = New CConcept
   Set oCompany.priorLabels = New Dictionary
   Set oCompany.parentArcs = New Dictionary
   Set oCompany.childArcs = New Dictionary
   Set oCompany.labels = New Dictionary
   Set oDocument.labels = New Dictionary
'------------------------------------------------
'INITIALIZE VARIABLES
'------------------------------------------------
   oJob.procNumber = 2
   iTableCount = 1
   bISAssigned = False
   iFactID = oToolbox.ids.Item("fact") + 1
   '------------------------------------------------------------------------
   'POPULATE DICTIONARIES
   '------------------------------------------------------------------------
   Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Populating company dictionaries"))
   '------------------------------------------------------------------------
   'populate company dictionaries
   '------------------------------------------------------------------------
   oCompany.GetDimensionArcs oCompany, oToolbox
   DoEvents
   oCompany.GetSegments oCompany, oToolbox
   DoEvents
   oCompany.GetLabels oCompany, oToolbox
   DoEvents
'------------------------------------------------
'QUERIES
'------------------------------------------------
   sPrmQExclusion(1) = "p_label"
'------------------------------------------------
'LOGIC
'------------------------------------------------
   sAccession = Replace(oDocument.accessionNumber, "-", "")
   oDocument.xbrlRoot = Replace(oDocument.xbrlRoot, "_cal", vbNullString)
   oDocument.xbrlRoot = Replace(oDocument.xbrlRoot, "_lab", vbNullString)
   oDocument.xbrlRoot = Replace(oDocument.xbrlRoot, "_pre", vbNullString)
   oDocument.xbrlRoot = Replace(oDocument.xbrlRoot, "_def", vbNullString)
   oDocument.xbrlPrefix = Split(oDocument.xbrlRoot, "-")(0)
   Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Downloading XBRL files from SEC.gov"))
   '------------------------------------------------------------------------
   'download instance file
   '------------------------------------------------------------------------
   sSource = "http://sec.gov/Archives/edgar/data/" & oCompany.CIK & "/" & sAccession & "/" & oDocument.xbrlRoot & ".xml"
   sDestination = "c:\AsReported\Docs\" & oDocument.xbrlRoot & ".xml"
   bComplete = DownloadFile(sSource, sDestination, OverwriteKill, "Error")
   If Not bComplete Then
      Application.Cursor = xlNorthwestArrow
      ProcessInstance = False
      Exit Function
   End If
   DoEvents
   '------------------------------------------------------------------------
   'download schema file
   '------------------------------------------------------------------------
   sSource = "http://sec.gov/Archives/edgar/data/" & oCompany.CIK & "/" & sAccession & "/" & oDocument.xbrlRoot & ".xsd"
   sDestination = "c:\AsReported\Docs\" & oDocument.xbrlRoot & ".xsd"
   bComplete = DownloadFile(sSource, sDestination, OverwriteKill, "Error")
   If Not bComplete Then
      Application.Cursor = xlNorthwestArrow
      ProcessInstance = False
      Exit Function
   End If
   DoEvents
   '------------------------------------------------------------------------
   'download calculation linkbase
   '------------------------------------------------------------------------
   sSource = "http://sec.gov/Archives/edgar/data/" & oCompany.CIK & "/" & sAccession & "/" & oDocument.xbrlRoot & "_cal.xml"
   sDestination = "c:\AsReported\Docs\" & oDocument.xbrlRoot & "_cal.xml"
   bComplete = DownloadFile(sSource, sDestination, OverwriteKill, "Error")
   If Not bComplete Then
      bCalcLinkbaseExists = False
   Else
      bCalcLinkbaseExists = True
   End If
   '------------------------------------------------------------------------
   'download label linkbase
   '------------------------------------------------------------------------
   sSource = "http://sec.gov/Archives/edgar/data/" & oCompany.CIK & "/" & sAccession & "/" & oDocument.xbrlRoot & "_lab.xml"
   sDestination = "c:\AsReported\Docs\" & oDocument.xbrlRoot & "_lab.xml"
   bComplete = DownloadFile(sSource, sDestination, OverwriteKill, "Error")
   If Not bComplete Then
      Application.Cursor = xlNorthwestArrow
      ProcessInstance = False
      Exit Function
   End If
   DoEvents
   '------------------------------------------------------------------------
   'download presentation linkbase
   '------------------------------------------------------------------------
   sSource = "http://sec.gov/Archives/edgar/data/" & oCompany.CIK & "/" & sAccession & "/" & oDocument.xbrlRoot & "_pre.xml"
   sDestination = "c:\AsReported\Docs\" & oDocument.xbrlRoot & "_pre.xml"
   bComplete = DownloadFile(sSource, sDestination, OverwriteKill, "Error")
   If Not bComplete Then
      Application.Cursor = xlNorthwestArrow
      ProcessInstance = False
      Exit Function
   End If
   DoEvents
   '------------------------------------------------------------------------
   'download definition linkbase
   '------------------------------------------------------------------------
   sSource = "http://sec.gov/Archives/edgar/data/" & oCompany.CIK & "/" & sAccession & "/" & oDocument.xbrlRoot & "_def.xml"
   sDestination = "c:\AsReported\Docs\" & oDocument.xbrlRoot & "_def.xml"
   bComplete = DownloadFile(sSource, sDestination, OverwriteKill, "Error")
   If Not bComplete Then
      bDefLinkbaseExists = False
   Else
      bDefLinkbaseExists = True
   End If
   '------------------------------------------------------------------------
   'populate object model
   '------------------------------------------------------------------------
   With oDocument
      '------------------------------------------------------------------------
      'label linkbase
      '------------------------------------------------------------------------
      Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Parsing label linkbase"))
      bReturn = .ParseLabelLinkbase(oLLinkbase, domDocument, oDocument, oCompany, oToolbox, oJob)
      If bReturn = False Then
         ProcessInstance = False
         GoTo proc_exit
      End If
      'Set oLabelLink = oLLinkbase.labelLinks.Item("1")
      '------------------------------------------------------------------------
      'schema
      '------------------------------------------------------------------------
      Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Parsing schema"))
      bReturn = .ParseSchema(oElementList, oPLinkbase, oCLinkbase, oLabelLink, domDocument, oDocument, oCompany, oToolbox, oJob)
      If bReturn = False Then
         ProcessInstance = False
         GoTo proc_exit
      End If
      '------------------------------------------------------------------------
      'definition linkbase
      '------------------------------------------------------------------------
      If bDefLinkbaseExists Then
         Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Parsing definition linkbase"))
         bReturn = .ParseDefinitionLinkbase(domDocument, oLabelLink, oDocument, oCompany, oToolbox, oJob)
         If bReturn = False Then
            ProcessInstance = False
            GoTo proc_exit
         End If
      End If
      '------------------------------------------------------------------------
      'presentation linkbase
      '------------------------------------------------------------------------
      Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Parsing presentation linkbase"))
      bReturn = .ParsePresentationLinkbase(oPLinkbase, oLabelLink, domDocument, oDocument, oCompany, oToolbox, oJob)
      If bReturn = False Then
         ProcessInstance = False
         GoTo proc_exit
      End If
      '------------------------------------------------------------------------
      'calculation linkbase
      '------------------------------------------------------------------------
      If bCalcLinkbaseExists Then
         Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Parsing calculation linkbase"))
         bReturn = .ParseCalculationLinkbase(oCLinkbase, domDocument, oDocument, oCompany, oToolbox)
         If bReturn = False Then
            ProcessInstance = False
            GoTo proc_exit
         End If
      End If
      '------------------------------------------------------------------------
      'instance
      '------------------------------------------------------------------------
      Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Parsing instance"))
      bReturn = .ParseInstance(domDocument, oDocument, oCompany, oToolbox, oJob, oUSGaap)
      If bReturn = False Then
         ProcessInstance = False
         GoTo proc_exit
      End If
   End With
   Set domDocument = Nothing
   '------------------------------------------------------------------------
   'process
   '------------------------------------------------------------------------
   Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Processing tables"))
   iTableCount = 0
   iTotalExtCount = 0
   For Each vPresLink In oPLinkbase.presentationLinks.Keys
      iSegCount = 0
      iFactCount = 0
      iRowCount = 0
      iExtCount = 0
      Set dctConcepts = New Dictionary
      Set oPLink = oPLinkbase.presentationLinks.Item(vPresLink)
         Set oCLink = oCLinkbase.calculationLinks.Item(vPresLink)
         Set oDataTable = oCompany.dataTables.Item(CStr(oPLink.role))
         If oPLink.linkType = "TEXTBLOCK" Then
            oDataTable.descriptor = "TEXTBLOCK"
            If oJob.dataTables.Exists(CStr(oDataTable.dataTableID)) Then
               oJob.dataTables.Item(CStr(oDataTable.dataTableID)).descriptor = "TEXTBLOCK"
            End If
         End If
         '------------------------------------------------------------------------
         'process arcs
         '------------------------------------------------------------------------
         iRowCount = 0
         iFactCount = 0
         For Each vPresArc In oPLink.presentationArcs.Keys
            Set oPArc = oPLink.presentationArcs.Item(vPresArc)
            iLinkToConceptID = 0
            iLinkFromConceptID = 0
            bLinkFromConceptExists = False
            '------------------------------------------------------------------------
            'create linkto concept
            '------------------------------------------------------------------------
            If oToolbox.concepts.Exists(oPLink.locators.Item(CStr(oPArc.linkTo)).href) Then
               Set oLinkToConcept = oToolbox.concepts.Item(oPLink.locators.Item(CStr(oPArc.linkTo)).href)
               Set oLinkFromConcept = oToolbox.concepts.Item(oPLink.locators.Item(CStr(oPArc.linkFrom)).href)
               '------------------------------------------------------------------------
               'calculation parent conceptID
               '------------------------------------------------------------------------
               If oLinkToConcept.abstract = False And Not oCLink.locators Is Nothing Then
                  iCalculationParentID = 0
                  If oCLink.locators.count > 0 Then
                     Set oCalcParentConcept = New CConcept
                     oCalcParentConcept.conceptID = 0
                     '------------------------------------------------------------------------
                     'calculation parent locator
                     '------------------------------------------------------------------------
                     For Each vLocator In oCLink.locators.Items
                        If vLocator.href = oLinkToConcept.identifier Then
                           Set oCalculationLocator = vLocator
                           Exit For
                        End If
                     Next vLocator
                     If Not oCalculationLocator Is Nothing Then
                        If oCLink.calculationArcs.Exists(CStr(oCalculationLocator.label)) Then
                           Set oCArc = oCLink.calculationArcs.Item(CStr(oCalculationLocator.label))
                           If oToolbox.concepts.Exists(CStr(oCLink.locators.Item(CStr(oCArc.linkFrom)).href)) Then
                              Set oCalcParentConcept = oToolbox.concepts.Item(CStr(oCLink.locators.Item(CStr(oCArc.linkFrom)).href))
                           Else
                              iCalculationParentID = 0
                           End If
                        Else
                           iCalculationParentID = 0
                        End If
                     Else
                        Set oCalcParentConcept = New CConcept
                        oCalcParentConcept.conceptID = 0
                     End If
                  Else
                     Set oCalcParentConcept = New CConcept
                     oCalcParentConcept.conceptID = 0
                  End If
               Else
                  Set oCalcParentConcept = New CConcept
                  oCalcParentConcept.conceptID = 0
               End If
               '------------------------------------------------------------------------
               'get linkTo label
               '------------------------------------------------------------------------
               If Len(oPArc.preferredLabel) > 0 Then
                  sLabelType = oPArc.preferredLabel
               Else
                  sLabelType = "label"
               End If
               sLabelCode = sLabelType & ":" & oLinkToConcept.identifier
               bLabelSaved = False
               If oDocument.labels.Exists(CStr(sLabelCode)) Then
               'If oLabelLink.labelGroups.Exists(CStr(oLinkToConcept.identifier)) Then
                  'Set oLabelGroup = oLabelLink.labelGroups.Item(CStr(oLinkToConcept.identifier))
                  '------------------------------------------------------------------------
                  'set labelType
                  '------------------------------------------------------------------------
                  Set oLabel = oDocument.labels.Item(CStr(sLabelCode))
                  '------------------------------------------------------------------------
                  'save lineItem
                  '------------------------------------------------------------------------
                  iRowCount = iRowCount + 1
                  If oLinkToConcept.companyID > 0 Then iExtCount = iExtCount + 1
                  Set oLineItem = New CLineItem
                  With oLineItem
                     oToolbox.ids.Item("lineItem") = oToolbox.ids.Item("lineItem") + 1
                     .lineItemID = oToolbox.ids.Item("lineItem")
                     .documentID = oDocument.documentID
                     .dataTableID = oDataTable.dataTableID
                     .conceptID = oLinkToConcept.conceptID
                     .abstractParentID = oLinkFromConcept.conceptID
                     .calculationParentID = oCalcParentConcept.conceptID
                     If .calculationParentID > 0 Then
                        .weight = oCArc.weight
                     End If
                     .sequence = iRowCount 'oPArc.order
                     .arcRoleID = oToolbox.arcRoles.Item(CStr(oPArc.ArcRole))
                     .preferredLabelID = oToolbox.labelRoles.Item(CStr(oPArc.preferredLabel))
                     .labelID = oLabel.labelID
                     .jobID = oJob.jobID
                  End With
                  oJob.lineItems.Add CStr(oLineItem.lineItemID), oLineItem
                  '------------------------------------------------------------------------
                  'save facts for linkTo conceptID
                  '------------------------------------------------------------------------
                  If oDocument.factGroups.Exists(CStr(oLinkToConcept.identifier)) Then
                     For Each vKey In oDocument.factGroups.Item(CStr(oLinkToConcept.identifier)).facts.Keys
                        bSkipFact = False
                        Set oFact = oDocument.factGroups.Item(CStr(oLinkToConcept.identifier)).facts.Item(vKey)
                        With oFact
                           If oDocument.contexts.Exists(CStr(.contextRef)) Then
                              '------------------------------------------------------------------------
                              'context period
                              '------------------------------------------------------------------------
                              Set oContext = oDocument.contexts.Item(CStr(.contextRef))
                              .conceptID = oLinkToConcept.conceptID
                              .periodID = oContext.period.periodID
                              .segmentID = oContext.segment.segmentID
                              '------------------------------------------------------------------------
                              'add oFact
                              '------------------------------------------------------------------------
                              iFactCount = iFactCount + 1
                              iFactsTotal = iFactsTotal + 1
                              '------------------------------------------------------------------------
                              'increment factID
                              '------------------------------------------------------------------------
                              oToolbox.ids.Item("fact") = oToolbox.ids.Item("fact") + 1
                              .factID = oToolbox.ids("fact")
                              .documentID = oDocument.documentID
                              .unitID = 1
                              If Len(.value) > 0 And Len(.value) < 31 Then
                                 If isNumeric(.value) Then
                                    .numericValue = CDbl(.value)
                                 Else
                                    .nonNumericValue = .value
                                 End If
                              End If
                              .jobID = oJob.jobID
                              oJob.facts.Add CStr(iFactsTotal), oFact
                              iFactID = iFactID + 1
                              Set oFact = Nothing
                           End If
                        End With
                     Next vKey
                     oDocument.factGroups.Remove (CStr(oLinkToConcept.identifier))
                  End If
               End If
            End If
         Next vPresArc
         iArcsTotal = iArcsTotal + iRowCount
         iDimensionsTotal = iDimensionsTotal + iSegCount
         iTotalExtCount = iTotalExtCount + iExtCount
         DoEvents
   Next vPresLink
   With grReadout
      '------------------------------------------------------------------------
      'commit dataset to server
      '------------------------------------------------------------------------
      Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Committing data to cloud storage"))
      oJob.CommitJob oJob, grStatus, oToolbox
   End With
'------------------------------------------------
'EXIT
'------------------------------------------------
   Properties.ReadoutGrid.CurRow = oToolbox.monitorRows.Item(CStr("Cleaning up"))
   ProcessInstance = True
proc_exit:
   '------------------------------------------------------------------------
   'Delete Files
   '------------------------------------------------------------------------
   Kill "c:\AsReported\Docs\" & oDocument.xbrlRoot & ".xml"
   Kill "c:\AsReported\Docs\" & oDocument.xbrlRoot & ".xsd"
   If bCalcLinkbaseExists Then
      Kill "c:\AsReported\Docs\" & oDocument.xbrlRoot & "_cal.xml"
   End If
   Kill "c:\AsReported\Docs\" & oDocument.xbrlRoot & "_lab.xml"
   Kill "c:\AsReported\Docs\" & oDocument.xbrlRoot & "_pre.xml"
   If bDefLinkbaseExists Then
      Kill "c:\AsReported\Docs\" & oDocument.xbrlRoot & "_def.xml"
   End If
   Properties.ReadoutGrid.CurRow = 0
'------------------------------------------------
'DECONSTRUCTORS
'------------------------------------------------
   Set frm = Nothing
   Set oInterface = Nothing
   Set rs = Nothing
   Set cmd = Nothing
   Set oPLinkbase = Nothing
   Set oCLinkbase = Nothing
   Set oLLinkbase = Nothing
   Set oElementList = Nothing
   Set oConcept = Nothing
   Set oLinkToConcept = Nothing
   Set oLinkFromConcept = Nothing
   Set oCalcParentConcept = Nothing
   Set oPLink = Nothing
   Set oPArc = Nothing
   Set oCLink = Nothing
   Set oCArc = Nothing
   Set oLabel = Nothing
   Set oDataTable = Nothing
   Set oLineItem = Nothing
   Set grReadout = Nothing
   Set domDocument = Nothing
   Set dctConcepts = Nothing
   Set oLocator = Nothing
   Set oLabelGroup = Nothing
   Set oLabelLink = Nothing
   Set oFact = Nothing
   Set oFact1 = Nothing
   Set oContext = Nothing
   Set oSegment = Nothing
   Set oLinkToLocator = Nothing
   Set oLinkFromLocator = Nothing
   Set oCalculationLocator = Nothing
   Set oPeriod = Nothing
'------------------------------------------------
'RESET CONDITIONS
'------------------------------------------------
   On Error GoTo 0
   Exit Function
'------------------------------------------------
'ERROR HANDLER
'------------------------------------------------
ProcessInstance_Error:

   MsgBox "Error " & Err.number & " (" & Err.description & ") in procedure ProcessInstance of Function CDocument"

End Function